class Calculator(val interactor: Interactor = Interactor()) {

    fun setFirstOperand() {
        interactor.askToSetFirstOperand()

        while (firstOperand == null) {
            firstOperand = interactor.getNumberFromUser()
        }
    }

    fun setSecondOperand() {
        interactor.askToSetSecondOperand()

        while (secondOperand == null) {
            secondOperand = interactor.getNumberFromUser()
        }
    }

    fun setOperation() {
        interactor.askToSetOperation()

        while (operation == null) {
            operation = interactor.getOperationFromUser()
        }
    }

    fun getResult() = try {
        interactor.showResult(result)
    } catch (e: CalculationDataException) {
        interactor.showError(e.message ?: "Something went wrong :(")
    }

    private var firstOperand: Int? = null

    private var secondOperand: Int? = null

    private var operation: Operation? = null

    private val result: Int
        get() {
            val firstOperand = firstOperand
            val secondOperand = secondOperand
            val operation = operation

            if (firstOperand == null || secondOperand == null || operation == null) {
                throw CalculationDataException("Calculation data is incorrect! Try again.")
            }

            return calculate(firstOperand, secondOperand, operation)
        }

    private fun calculate(
        firstOperand: Int,
        secondOperand: Int,
        operation: Operation,
    ) = when (operation) {
        Operation.PLUS -> firstOperand + secondOperand
        Operation.MINUS -> firstOperand - secondOperand
        Operation.MULTIPLICATION -> firstOperand * secondOperand
        Operation.DIVISION -> firstOperand / secondOperand
        Operation.CYLINDER_VOLUME ->{
            if(firstOperand <= 0 || secondOperand <=0) throw CalculationDataException("ERROR: Both r and h should be positive numbers!")
            (Math.PI * firstOperand * firstOperand * secondOperand).toInt()
        }
    }
}