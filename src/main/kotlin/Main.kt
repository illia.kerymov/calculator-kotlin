fun main() {
    val calculator = Calculator()

    with(calculator) {
        setFirstOperand()
        setSecondOperand()
        setOperation()
        getResult()
    }
}