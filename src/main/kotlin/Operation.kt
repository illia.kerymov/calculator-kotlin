enum class Operation(val code: Int) {
    PLUS(1),
    MINUS(2),
    MULTIPLICATION(3),
    DIVISION(4),
    CYLINDER_VOLUME(5)
}