class Interactor {

    fun askToSetFirstOperand() {
        println("Set first operand, please: ")
    }

    fun askToSetSecondOperand() {
        println("Set second operand, please: ")
    }

    fun askToSetOperation() {
        val operationInputInstruction = Operation.entries
            .toList()
            .joinToString { "${it.code} - ${it.name}" }

        println("Set operation, please: ")
        println(operationInputInstruction)
    }

    fun getNumberFromUser(): Int? {
        val input = readlnOrNull()
        if (input == null) {
            println("Input is undefined! Try again")
            return null
        }

        val number = input.toIntOrNull()
        if (number == null) {
            println("Type mismatch! Try again")
            return null
        }

        return number
    }

    fun getOperationFromUser(): Operation? {
        val input = readlnOrNull()
        if (input == null) {
            println("Input is undefined! Try again")
            return null
        }

        val operation = input.toOperationOrNull()
        if (operation == null) {
            println("Unsupported operation code! Try again")
            return null
        }

        return operation
    }

    fun showResult(result: Int) {
        println("Result: $result")
    }

    fun showError(message: String) {
        println(message)
    }
}

private fun String.toOperationOrNull(): Operation? {
    val operationCode = this.toIntOrNull()

    return Operation.entries.find { it.code == operationCode }
}